﻿using System.Collections.Generic;
using ClientDependency.Core;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors;

namespace AngularGoogleMaps
{
	public enum CoordinatesBehavour
	{
		Hide = 0,
		Show = 1,
		Edit = 2
	}

	[PropertyEditor("AngularGoogleMaps", "Angular Google Maps", "/App_Plugins/AngularGoogleMaps/1.0.6/view.html", ValueType = "TEXT")]
	[PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/AngularGoogleMaps/1.0.6/controller.js")]
	[PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/AngularGoogleMaps/1.0.6/angular-google-maps.js")]
	[PropertyEditorAsset(ClientDependencyType.Css, "/App_Plugins/AngularGoogleMaps/1.0.6/view.css")]
	public class AGMPropertyEditor : PropertyEditor
	{
		protected override PreValueEditor CreatePreValueEditor()
		{
			return new AGMPreValueEditor();
		}

		public AGMPropertyEditor()
		{
            _defaultPreVals = new Dictionary<string, object>
            {
                {"defaultLocation", "0,0,1"},
                {"height", 400},
				{"coordinatesBehavour", (int) CoordinatesBehavour.Edit},
				{"iconUrl", "Red marker"}
            };
		}

        private IDictionary<string, object> _defaultPreVals;
		public override IDictionary<string, object> DefaultPreValues
		{
			get { return _defaultPreVals; }
			set { _defaultPreVals = value; }
		}

		internal class AGMPreValueEditor : PreValueEditor
		{
			[PreValueField("defaultLocation", "Default Location", "/App_Plugins/AngularGoogleMaps/1.0.6/defaultlocation.html",
				Description = "Enter the default location for new map: latitude,longitude,zoom")]
			public string DefaultLocation { get; set; }

			[PreValueField("height", "Map Height", "number",
				Description = "Enter the height of the Google map in pixels. 400 is default")]
			public int Height { get; set; }

			[PreValueField("hideSearch", "Hide Search", "boolean",
				Description = "Hide the search feature")]
			public bool HideSearch { get; set; }

			[PreValueField("hideLabel", "Hide Label", "boolean",
				Description = "Hide the Umbraco property title and description, making the map span the entire page width")]
			public bool HideLabel { get; set; }

			[PreValueField("coordinatesBehavour", "Coordinates Behavour", "/App_Plugins/AngularGoogleMaps/1.0.6/coordinatesbehavour.html", 
				Description = "Choose whether selected coordinates are shown and/or editable by the content editor")]
			public CoordinatesBehavour coordinatesBehavour { get; set; }

			[PreValueField("iconUrl", "Marker Icon Url", "/App_Plugins/AngularGoogleMaps/1.0.6/iconurl.html",
				Description = "Enter icon url or select predefined value from dropdown list")]
			public string IconUrl { get; set; }

			//[PreValueField("iconSize", "Marker Icon Size", "/App_Plugins/AngularGoogleMaps/1.0.6/iconsize.html",
			//	Description = "Enter icon size in pixel in format width,height")]
			//public string IconSize { get; set; }

			//[PreValueField("iconAnchorPoint", "Marker Icon Anchor Point", "/App_Plugins/AngularGoogleMaps/1.0.6/iconanchorpoint.html",
			//	Description = "Enter icon anchor point (in format top pixels, left pixels) or select predefined value from dropdown list")]
			//public string IconAnchorPoint { get; set; }

		}
	}
}
